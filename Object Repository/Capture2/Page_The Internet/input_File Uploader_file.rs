<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_File Uploader_file</name>
   <tag></tag>
   <elementGuidId>16870742-b9a7-4036-bf74-15c8b5081f0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file-upload']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#file-upload</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>575e1a36-ed57-4636-9f64-ec41294717a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-upload</value>
      <webElementGuid>5377c151-3fc5-4e4c-81b1-3f506f738919</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>4ad77a1e-7a11-4f23-b74b-adf4096f5001</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>27ea2977-a707-44b7-afa3-0def3016c6d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file-upload&quot;)</value>
      <webElementGuid>e28f422a-af4c-4c6e-a002-d199933b64e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file-upload']</value>
      <webElementGuid>684022bf-8623-45aa-99e3-4aef12bc739e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/form/input</value>
      <webElementGuid>6386c8cc-d029-4540-a3e9-77df8867f140</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>2f052c4c-ba58-4e15-961c-d2cb75849399</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file-upload' and @type = 'file' and @name = 'file']</value>
      <webElementGuid>3e7b1682-3cfe-4aea-af22-fce94eca8329</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
