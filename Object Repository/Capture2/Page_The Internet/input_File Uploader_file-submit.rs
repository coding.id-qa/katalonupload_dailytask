<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_File Uploader_file-submit</name>
   <tag></tag>
   <elementGuidId>6ba0264d-4d08-486d-aaa3-bf79ee13b008</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file-submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#file-submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>92df676e-4b57-4a58-9f90-0f17383192d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>da5b0303-e8e8-4bd5-ad6e-f3bfc6bb1527</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-submit</value>
      <webElementGuid>20ba9ebe-8642-44fb-8c2d-187166f82d87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>1d805926-7499-47ef-b7d1-9d57059292e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Upload</value>
      <webElementGuid>b814e5ba-16b9-4d5b-9c8b-61bcb50b7743</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file-submit&quot;)</value>
      <webElementGuid>1005d163-6eca-4430-92b8-19b94bdd05f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file-submit']</value>
      <webElementGuid>1560aa07-01f5-49bb-a22f-a3d4d834e4e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/form/input[2]</value>
      <webElementGuid>88c9ae20-434b-48a7-ab50-921388db4d42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>83b785c0-3c1b-4c54-8543-012c33079742</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file-submit' and @type = 'submit']</value>
      <webElementGuid>2bc739b2-5b76-40c4-a36b-fea458274865</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
