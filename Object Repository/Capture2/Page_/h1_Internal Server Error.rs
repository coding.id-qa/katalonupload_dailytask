<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Internal Server Error</name>
   <tag></tag>
   <elementGuidId>2616ee5f-6b6f-4483-9fbd-6190c47efa0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='Internal Server Error']/parent::*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>f0d69748-3ca0-4e3b-a560-fcf28deef57c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Internal Server Error</value>
      <webElementGuid>fed30e3f-e128-4734-b548-02fda5861862</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/h1[1]</value>
      <webElementGuid>5dcb9dd9-474e-41c9-a069-973acceca7de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::h1[1]</value>
      <webElementGuid>c7e18a6d-2113-4e6f-b2e3-c034e86ebaa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::h1[1]</value>
      <webElementGuid>f7e94795-e6ce-4fcb-9514-8f5b815633fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Internal Server Error']/parent::*</value>
      <webElementGuid>13922a7d-c555-4edb-8849-cb3c594858a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>b747bc04-f87b-41d3-ae14-f3deba9dd45f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Internal Server Error' or . = 'Internal Server Error')]</value>
      <webElementGuid>e358c00a-1e9a-4a57-afb6-c6ab5cb14f0e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
