<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Data Driven</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>5670a25b-da0c-429c-8d73-f1be7b00e809</testSuiteGuid>
   <testCaseLink>
      <guid>71d2ab36-633e-4588-8d89-e0c22ff4cdd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>6195b568-1de1-4454-a64e-0795a9c91ea4</id>
         <masked>false</masked>
         <name>File_Path</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Data Driven</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7e759241-3d4c-4cfc-a596-12c4a85e2d7d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7e759241-3d4c-4cfc-a596-12c4a85e2d7d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>File</value>
         <variableId>6195b568-1de1-4454-a64e-0795a9c91ea4</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
